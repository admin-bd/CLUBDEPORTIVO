CREATE DATABASE CLUBDEPORTIVO

USE CLUBDEPORTIVO

CREATE SCHEMA Gerencia
CREATE SCHEMA Cobranza
CREATE SCHEMA Registro

CREATE TABLE Gerencia.Membresia(
	IdMembresia			BIGINT IDENTITY(1,1) NOT NULL,
	Descripcion			VARCHAR(200) NOT NULL,
	Precio				REAL NOT NULL,

	CONSTRAINT PK_MEMBRESIA PRIMARY KEY (IdMembresia),
	CONSTRAINT UK_DESCRIPCION UNIQUE(Descripcion)
)

CREATE TABLE Gerencia.Clases(
	IdClase				BIGINT IDENTITY(1,1) NOT NULL,
	IdMembresia			BIGINT NOT NULL,
	Cupo				INT NOT NULL,

	CONSTRAINT PK_CLASES PRIMARY KEY (IdClase),
	CONSTRAINT FK_MEMBRESIA1 FOREIGN KEY(IdMembresia) REFERENCES Gerencia.Membresia(IdMembresia)
)

CREATE TABLE Gerencia.Instructor(
	IdInstructor		BIGINT IDENTITY(1,1) NOT NULL,
	Nombre				VARCHAR(100) NOT NULL,
	Domicilio			VARCHAR(200) NOT NULL,
	Edad				INT NOT NULL,
	Telefono			VARCHAR(50) NOT NULL,

	CONSTRAINT PK_INSTRUCTOR PRIMARY KEY (IdInstructor)
)

CREATE TABLE Gerencia.Horarios(
	IdHorario			BIGINT IDENTITY(1,1) NOT NULL,
	IdClase				BIGINT NOT NULL,
	IdInstructor		BIGINT NOT NULL,
	HoraInicio			TIME NOT NULL,
	HoraFin				TIME NOT NULL,
	Dias				VARCHAR NOT NULL,

	CONSTRAINT PK_HORARIOS	PRIMARY KEY (IdHorario),
	CONSTRAINT FK_CLASES FOREIGN KEY(IdClase) REFERENCES Gerencia.Clases(IdClase),
	CONSTRAINT FK_INSTRUCTOR FOREIGN KEY(IdInstructor) REFERENCES Gerencia.Instructor(IdInstructor)
)

CREATE RULE ReglaHorario AS @Dias IN ('L-M-Mi-J-V','L-Mi-V','M-J-S','M-J','S')
/*Liga la regla a un campo*/
EXEC sp_bindrule 'ReglaHorario','Gerencia.Horarios.Dias'

CREATE TABLE Gerencia.Socio(
	IdSocio				BIGINT IDENTITY(1,1) NOT NULL,
	Nombre				VARCHAR(100) NOT NULL,
	Telefono			VARCHAR(50)	NOT NULL,
	Membresia			BIGINT NOT NULL,
	FechaInicio			DATE NOT NULL,
	Domicilio			VARCHAR(200) NOT NULL,
	EDAD				INT NOT NULL,
	FechaNacimiento		DATE NOT NULL,

	CONSTRAINT PK_SOCIO PRIMARY KEY (IdSocio),
	CONSTRAINT FK_MEMBRESIA2 FOREIGN KEY(Membresia) REFERENCES Gerencia.Membresia(IdMembresia)
)

CREATE TABLE Registro.Asistencia(
	IdAsistencia		BIGINT IDENTITY(1,1) NOT NULL,
	IdSocio				BIGINT NOT NULL,
	Fecha				DATE NOT NULL,
	HoraEntrada			TIME NOT NULL,
	HoraSalida			TIME NOT NULL,

	CONSTRAINT PK_ASISTENCIA PRIMARY KEY (IdAsistencia),
	CONSTRAINT FK_SOCIO1 FOREIGN KEY(IdSocio) REFERENCES Gerencia.Socio(IdSocio)
)

CREATE TABLE Registro.AsistenciaClase(
	IdAsistencia		BIGINT NOT NULL,
	IdHorario			BIGINT NOT NULL,

	CONSTRAINT FK_ASISTENCIA FOREIGN KEY(IdAsistencia) REFERENCES Registro.Asistencia(IdAsistencia),
	CONSTRAINT FK_HORARIO FOREIGN KEY(IdHorario) REFERENCES Gerencia.Horarios(IdHorario)
)

CREATE TABLE Cobranza.Pagos(
	IdPagos				BIGINT IDENTITY(1,1) NOT NULL,
	Fecha				DATE NOT NULL,
	IdSocio				BIGINT NOT NULL,
	CantPagada			REAL NOT NULL,

	CONSTRAINT PK_PAGOS PRIMARY KEY (IdPagos),
	CONSTRAINT FK_SOCIO2 FOREIGN KEY(IdSocio) REFERENCES Gerencia.Socio(IdSocio)
)


CREATE TRIGGER ActualizaAdeudo
	ON Registro.Asistencia 
	FOR INSERT
	AS
	BEGIN
		DECLARE @precio REAL
		DECLARE @fechaAux DATE
		DECLARE @fechaAux2 DATE
		SELECT @fechaAux = Fecha FROM inserted  
		SELECT @fechaAux2 = Cobranza.Pagos.Fecha FROM Cobranza.Pagos INNER JOIN inserted ON Cobranza.Pagos.IdSocio=inserted.IdSocio

		IF @fechaAux >= DATEADD(year, 1, @fechaAux2)
		SET @precio = (SELECT Precio FROM Gerencia.Socio INNER JOIN Gerencia.Membresia ON Gerencia.Socio.Membresia=Gerencia.Membresia.IdMembresia);
		ELSE
		SET @precio ='0';
		UPDATE Cobranza.Pagos SET CantPagada=CantPagada-@precio WHERE Cobranza.Pagos.IdSocio=IdSocio;
	END;

CREATE TRIGGER ChecarClase
	ON Registro.AsistenciaClase 
	FOR INSERT
	AS
	BEGIN
		DECLARE @clase BIGINT
		DECLARE @mem BIGINT
		DECLARE @cont INT
		SELECT @clase = IdHorario FROM inserted
		SELECT @mem = Membresia FROM Registro.Asistencia INNER JOIN inserted ON Registro.Asistencia.IdAsistencia=inserted.IdAsistencia INNER JOIN Gerencia.Socio ON Registro.Asistencia.IdSocio=Gerencia.Socio.IdSocio
		SELECT @cont = COUNT(Gerencia.Horarios.IdClase) FROM Gerencia.Clases INNER JOIN Gerencia.Horarios ON Gerencia.Clases.IdClase=Gerencia.Horarios.IdClase WHERE IdMembresia=@mem AND IdHorario=@clase

		IF @cont = 0
			RAISERROR('No pertences a la clase que deseas ingresar, consulta tu tipo de membres�a',10,1)
			ROLLBACK TRANSACTION
	END;

CREATE TRIGGER NuevoSocio
	ON Gerencia.Socio
	FOR INSERT
	AS
	BEGIN
		DECLARE @precio REAL
		DECLARE @socio BIGINT
		SELECT @socio = IdSocio FROM inserted
		SELECT @precio = 600
		INSERT INTO Cobranza.Pagos(Fecha,IdSocio,CantPagada) VALUES (CONVERT (date, GETDATE()),@socio,-@precio)
	END;
