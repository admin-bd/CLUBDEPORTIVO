﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubDeportivo
{
    public partial class SociosForm : Form
    {
        public int indiceMenu = 0;

        public SociosForm()
        {
            InitializeComponent();
        }

        private void sociosSocio_Click(object sender, EventArgs e)
        {
            indiceMenu = 1;
            this.Close();
        }

        private void clasesSocio_Click(object sender, EventArgs e)
        {
            indiceMenu = 2;
            this.Close();
        }

        private void instructoresSocio_Click(object sender, EventArgs e)
        {
            indiceMenu = 3;
            this.Close();
        }

        private void membresiasSocio_Click(object sender, EventArgs e)
        {
            indiceMenu = 4;
            this.Close();
        }

        private void asistenciaSocio_Click(object sender, EventArgs e)
        {
            indiceMenu = 5;
            this.Close();
        }

        private void pagosSocio_Click(object sender, EventArgs e)
        {
            indiceMenu = 6;
            this.Close();
        }
    }
}
