﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubDeportivo
{
    public partial class InstructoresForm: Form
    {
        public int indiceMenu;
        private nuevoInstructor altaInstructor;

        public InstructoresForm()
        {
            InitializeComponent();
            indiceMenu = 0;
        }

        private void sociosInstructores_Click(object sender, EventArgs e)
        {
            indiceMenu = 1;
            this.Close();
        }

        private void clasesInstructores_Click(object sender, EventArgs e)
        {
            indiceMenu = 2;
            this.Close();
        }

        private void instructoresInstructores_Click(object sender, EventArgs e)
        {
            indiceMenu = 3;
            this.Close();
        }

        private void membresiasInstructores_Click(object sender, EventArgs e)
        {
            indiceMenu = 4;
            this.Close();
        }

        private void asistenciaInstructores_Click(object sender, EventArgs e)
        {
            indiceMenu = 5;
            this.Close();
        }

        private void pagosInstructores_Click(object sender, EventArgs e)
        {
            indiceMenu = 6;
            this.Close();
        }

        private void homeInstructores_Click(object sender, EventArgs e)
        {
            indiceMenu = 7;
            this.Close();
        }

        private void insertarInstructor_Click(object sender, EventArgs e)
        {
            altaInstructor = new nuevoInstructor();

            altaInstructor.ShowDialog();
        }
    }
}
