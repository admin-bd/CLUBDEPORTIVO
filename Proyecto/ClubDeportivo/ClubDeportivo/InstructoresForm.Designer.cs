﻿namespace ClubDeportivo
{
    partial class InstructoresForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InstructoresForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.sociosInstructores = new System.Windows.Forms.ToolStripButton();
            this.clasesInstructores = new System.Windows.Forms.ToolStripButton();
            this.instructoresInstructores = new System.Windows.Forms.ToolStripButton();
            this.membresiasInstructores = new System.Windows.Forms.ToolStripButton();
            this.asistenciaInstructores = new System.Windows.Forms.ToolStripButton();
            this.pagosInstructores = new System.Windows.Forms.ToolStripButton();
            this.homeInstructores = new System.Windows.Forms.ToolStripButton();
            this.eliminarInstructor = new System.Windows.Forms.Button();
            this.actualizarInstructor = new System.Windows.Forms.Button();
            this.insertarInstructor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.instructoresDataGridView = new System.Windows.Forms.DataGridView();
            this.IdInstructor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.domicilio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.edad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.instructoresDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(70)))), ((int)(((byte)(79)))));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(55, 50);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sociosInstructores,
            this.clasesInstructores,
            this.instructoresInstructores,
            this.membresiasInstructores,
            this.asistenciaInstructores,
            this.pagosInstructores,
            this.homeInstructores});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(174, 741);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // sociosInstructores
            // 
            this.sociosInstructores.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sociosInstructores.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.sociosInstructores.Image = ((System.Drawing.Image)(resources.GetObject("sociosInstructores.Image")));
            this.sociosInstructores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.sociosInstructores.Name = "sociosInstructores";
            this.sociosInstructores.Size = new System.Drawing.Size(171, 54);
            this.sociosInstructores.Text = "Socios";
            this.sociosInstructores.Click += new System.EventHandler(this.sociosInstructores_Click);
            // 
            // clasesInstructores
            // 
            this.clasesInstructores.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clasesInstructores.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clasesInstructores.Image = ((System.Drawing.Image)(resources.GetObject("clasesInstructores.Image")));
            this.clasesInstructores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clasesInstructores.Name = "clasesInstructores";
            this.clasesInstructores.Size = new System.Drawing.Size(171, 54);
            this.clasesInstructores.Text = "Clases";
            this.clasesInstructores.Click += new System.EventHandler(this.clasesInstructores_Click);
            // 
            // instructoresInstructores
            // 
            this.instructoresInstructores.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instructoresInstructores.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.instructoresInstructores.Image = ((System.Drawing.Image)(resources.GetObject("instructoresInstructores.Image")));
            this.instructoresInstructores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.instructoresInstructores.Name = "instructoresInstructores";
            this.instructoresInstructores.Size = new System.Drawing.Size(171, 54);
            this.instructoresInstructores.Text = "Instructores";
            this.instructoresInstructores.Click += new System.EventHandler(this.instructoresInstructores_Click);
            // 
            // membresiasInstructores
            // 
            this.membresiasInstructores.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.membresiasInstructores.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.membresiasInstructores.Image = ((System.Drawing.Image)(resources.GetObject("membresiasInstructores.Image")));
            this.membresiasInstructores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.membresiasInstructores.Name = "membresiasInstructores";
            this.membresiasInstructores.Size = new System.Drawing.Size(171, 54);
            this.membresiasInstructores.Text = "Membresías";
            this.membresiasInstructores.Click += new System.EventHandler(this.membresiasInstructores_Click);
            // 
            // asistenciaInstructores
            // 
            this.asistenciaInstructores.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asistenciaInstructores.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.asistenciaInstructores.Image = ((System.Drawing.Image)(resources.GetObject("asistenciaInstructores.Image")));
            this.asistenciaInstructores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.asistenciaInstructores.Name = "asistenciaInstructores";
            this.asistenciaInstructores.Size = new System.Drawing.Size(171, 54);
            this.asistenciaInstructores.Text = "Asistencia";
            this.asistenciaInstructores.Click += new System.EventHandler(this.asistenciaInstructores_Click);
            // 
            // pagosInstructores
            // 
            this.pagosInstructores.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pagosInstructores.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pagosInstructores.Image = ((System.Drawing.Image)(resources.GetObject("pagosInstructores.Image")));
            this.pagosInstructores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pagosInstructores.Name = "pagosInstructores";
            this.pagosInstructores.Size = new System.Drawing.Size(171, 54);
            this.pagosInstructores.Text = "Pagos";
            this.pagosInstructores.Click += new System.EventHandler(this.pagosInstructores_Click);
            // 
            // homeInstructores
            // 
            this.homeInstructores.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homeInstructores.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.homeInstructores.Image = ((System.Drawing.Image)(resources.GetObject("homeInstructores.Image")));
            this.homeInstructores.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.homeInstructores.Name = "homeInstructores";
            this.homeInstructores.Size = new System.Drawing.Size(171, 54);
            this.homeInstructores.Text = "Home";
            this.homeInstructores.Click += new System.EventHandler(this.homeInstructores_Click);
            // 
            // eliminarInstructor
            // 
            this.eliminarInstructor.BackColor = System.Drawing.Color.White;
            this.eliminarInstructor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eliminarInstructor.Location = new System.Drawing.Point(844, 140);
            this.eliminarInstructor.Name = "eliminarInstructor";
            this.eliminarInstructor.Size = new System.Drawing.Size(135, 39);
            this.eliminarInstructor.TabIndex = 7;
            this.eliminarInstructor.Text = "Eliminar";
            this.eliminarInstructor.UseVisualStyleBackColor = false;
            // 
            // actualizarInstructor
            // 
            this.actualizarInstructor.BackColor = System.Drawing.Color.White;
            this.actualizarInstructor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actualizarInstructor.Location = new System.Drawing.Point(615, 140);
            this.actualizarInstructor.Name = "actualizarInstructor";
            this.actualizarInstructor.Size = new System.Drawing.Size(135, 39);
            this.actualizarInstructor.TabIndex = 8;
            this.actualizarInstructor.Text = "Actualizar";
            this.actualizarInstructor.UseVisualStyleBackColor = false;
            // 
            // insertarInstructor
            // 
            this.insertarInstructor.BackColor = System.Drawing.Color.White;
            this.insertarInstructor.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertarInstructor.Location = new System.Drawing.Point(378, 140);
            this.insertarInstructor.Name = "insertarInstructor";
            this.insertarInstructor.Size = new System.Drawing.Size(135, 39);
            this.insertarInstructor.TabIndex = 9;
            this.insertarInstructor.Text = "Insertar";
            this.insertarInstructor.UseVisualStyleBackColor = false;
            this.insertarInstructor.Click += new System.EventHandler(this.insertarInstructor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(70)))), ((int)(((byte)(79)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(588, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(190, 47);
            this.label1.TabIndex = 6;
            this.label1.Text = "Instructores";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // instructoresDataGridView
            // 
            this.instructoresDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(171)))), ((int)(((byte)(172)))));
            this.instructoresDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.instructoresDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdInstructor,
            this.nombre,
            this.domicilio,
            this.edad,
            this.telefono});
            this.instructoresDataGridView.Location = new System.Drawing.Point(246, 216);
            this.instructoresDataGridView.Name = "instructoresDataGridView";
            this.instructoresDataGridView.Size = new System.Drawing.Size(923, 406);
            this.instructoresDataGridView.TabIndex = 5;
            // 
            // IdInstructor
            // 
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IdInstructor.DefaultCellStyle = dataGridViewCellStyle1;
            this.IdInstructor.HeaderText = "ID";
            this.IdInstructor.Name = "IdInstructor";
            // 
            // nombre
            // 
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombre.DefaultCellStyle = dataGridViewCellStyle2;
            this.nombre.HeaderText = "Nombre";
            this.nombre.Name = "nombre";
            // 
            // domicilio
            // 
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.domicilio.DefaultCellStyle = dataGridViewCellStyle3;
            this.domicilio.HeaderText = "Domicilio";
            this.domicilio.Name = "domicilio";
            // 
            // edad
            // 
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edad.DefaultCellStyle = dataGridViewCellStyle4;
            this.edad.HeaderText = "Edad";
            this.edad.Name = "edad";
            // 
            // telefono
            // 
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.telefono.DefaultCellStyle = dataGridViewCellStyle5;
            this.telefono.HeaderText = "Teléfono";
            this.telefono.Name = "telefono";
            // 
            // InstructoresForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.ControlBox = false;
            this.Controls.Add(this.eliminarInstructor);
            this.Controls.Add(this.actualizarInstructor);
            this.Controls.Add(this.insertarInstructor);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.instructoresDataGridView);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "InstructoresForm";
            this.Text = "Next Step - Sport Club";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.instructoresDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton sociosInstructores;
        private System.Windows.Forms.ToolStripButton clasesInstructores;
        private System.Windows.Forms.ToolStripButton instructoresInstructores;
        private System.Windows.Forms.ToolStripButton membresiasInstructores;
        private System.Windows.Forms.ToolStripButton asistenciaInstructores;
        private System.Windows.Forms.ToolStripButton pagosInstructores;
        private System.Windows.Forms.ToolStripButton homeInstructores;
        private System.Windows.Forms.Button eliminarInstructor;
        private System.Windows.Forms.Button actualizarInstructor;
        private System.Windows.Forms.Button insertarInstructor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView instructoresDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdInstructor;
        private System.Windows.Forms.DataGridViewTextBoxColumn nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn domicilio;
        private System.Windows.Forms.DataGridViewTextBoxColumn edad;
        private System.Windows.Forms.DataGridViewTextBoxColumn telefono;
    }
}