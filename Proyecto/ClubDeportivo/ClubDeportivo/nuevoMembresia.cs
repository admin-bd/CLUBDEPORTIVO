﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ClubDeportivo
{
    public partial class nuevoMembresia : Form
    {
        public Int32 indice = 0;

        public nuevoMembresia()
        {
            InitializeComponent();
        }

        private void membresiaBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.membresiaBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.cLUBDEPORTIVODataSet);

        }

        private void nuevoMembresia_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'cLUBDEPORTIVODataSet.Membresia' Puede moverla o quitarla según sea necesario.
            this.membresiaTableAdapter.Fill(this.cLUBDEPORTIVODataSet.Membresia);

        }

        private void aceptar_Click(object sender, EventArgs e)
        {
            if (indice == 0)
            {
                SqlConnection n = new SqlConnection("Data Source=LUIS\\LUIS;Initial Catalog=CLUBDEPORTIVO;Integrated Security=True");
                n.Open();
                string auxPrecio = precio.Value.ToString();
                auxPrecio = auxPrecio.Replace(',', '.');
                SqlCommand cmd = new SqlCommand("INSERT INTO Gerencia.Membresia (Descripcion,Precio) VALUES('" + descripcion.Text + "','" + auxPrecio + "')", n);
                cmd.ExecuteNonQuery();
            }
            else
            {
                SqlConnection n = new SqlConnection("Data Source=LUIS\\LUIS;Initial Catalog=CLUBDEPORTIVO;Integrated Security=True");
                n.Open();
                string auxPrecio = precio.Value.ToString();
                auxPrecio = auxPrecio.Replace(',', '.');
                SqlCommand cmd = new SqlCommand("UPDATE Gerencia.Membresia SET Descripcion='" + descripcion.Text + "',Precio='" + auxPrecio + "' WHERE IdMembresia="+indice, n);
                cmd.ExecuteNonQuery();
            }
        }
    }
}
