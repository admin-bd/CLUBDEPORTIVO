﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.Data;

namespace DataBaseManager
{
    class ServerConnection
    {
        private SqlConnection connection;
        private SqlCommand command;

        public ServerConnection()
        {
            createServerConnection();
        }

        /// <summary>
        /// Create a new connection with SQL server
        /// </summary>
        public void createServerConnection()
        {
            connection = new SqlConnection();
            command = new SqlCommand();
            connection.ConnectionString = @"Data Source=ESTEFANIA-PC\MSSQLSERVERWT;Initial Catalog=master;Integrated Security=True";
            openConnection();
        }

        /// <summary>
        /// Open the connection with the server
        /// </summary>
        public void openConnection()
        {
            try {
                connection.Open();
            }
            catch(Exception ex) //when (ex is InvalidOperationException || ex is OleDbException)
            {
                MessageBox.Show("The connection can not open \n" + ex.Message );
            }
        }

        /// <summary>
        /// Close the connection with the server
        /// </summary>
        public void closeConnection()
        {
            connection.Close();
        }


        /// <summary>
        /// Execute the command in the string
        /// </summary>
        /// <param name="commandString">String of the command to execute</param>
        public bool executeCommand(string commandString)
        {
            bool res = true;
            command.CommandText = commandString;
            command.Connection = connection;
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                res = false;
            }
            return res;
        }

        public void executeCommand(string commandString, DataGridView dataGrid)
        {
            command.CommandText = commandString;
            command.Connection = connection;
            try
            {
                SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
                DataTable dt = new DataTable(); ;
                dataAdapter.Fill(dt);
                dataGrid.DataSource = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show("No se pudo realizar el llenado del datagrid. \n" + ex.Message);
            }
        }
    }
}
