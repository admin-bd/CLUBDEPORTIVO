﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace ClubDeportivo
{
    public partial class MembresiasForm : Form
    {
        public int indiceMenu = 0;
        private nuevoMembresia membresia;

        public MembresiasForm()
        {
            InitializeComponent();
            membresia = new nuevoMembresia();
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }

        private void membresiaBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.membresiaBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.cLUBDEPORTIVODataSet);

        }

        private void MembresiasForm_Load(object sender, EventArgs e)
        {
            // TODO: esta línea de código carga datos en la tabla 'cLUBDEPORTIVODataSet.Membresia' Puede moverla o quitarla según sea necesario.
            //this.membresiaTableAdapter.Fill(this.cLUBDEPORTIVODataSet.Membresia);

        }

        private void insertarMembresia_Click(object sender, EventArgs e)
        {
            membresia.indice = 0;
            membresia.descripcion.Text = "";
            membresia.precio.Value=0;
            membresia.ShowDialog();

            membresiaDataGridView.Update();
            this.membresiaTableAdapter.Fill(this.cLUBDEPORTIVODataSet.Membresia);
        }

        private void actualizarMembresia_Click(object sender, EventArgs e)
        {
            membresia.indice = System.Convert.ToInt32(membresiaDataGridView.CurrentRow.Cells[0].Value.ToString());
            membresia.descripcion.Text = membresiaDataGridView.CurrentRow.Cells[1].Value.ToString();
            membresia.precio.Value = System.Convert.ToDecimal(membresiaDataGridView.CurrentRow.Cells[2].Value.ToString());
            membresia.ShowDialog();

            membresiaDataGridView.Update();
            this.membresiaTableAdapter.Fill(this.cLUBDEPORTIVODataSet.Membresia);
        }

        private void eliminarMembresia_Click(object sender, EventArgs e)
        {
            SqlConnection n = new SqlConnection("Data Source=LUIS\\LUIS;Initial Catalog=CLUBDEPORTIVO;Integrated Security=True");
            n.Open();
            Int32 indice = System.Convert.ToInt32(membresiaDataGridView.CurrentRow.Cells[0].Value.ToString());

            SqlCommand cmd = new SqlCommand("DELETE FROM Gerencia.Membresia WHERE IdMembresia=" + indice, n);
            cmd.ExecuteNonQuery();

            membresiaDataGridView.Update();
            this.membresiaTableAdapter.Fill(this.cLUBDEPORTIVODataSet.Membresia);
        }

        private void sociosMembresia_Click(object sender, EventArgs e)
        {
            indiceMenu = 1;
            this.Close();
        }

        private void clasesMembresia_Click(object sender, EventArgs e)
        {
            indiceMenu = 2;
            this.Close();
        }

        private void instructoresMembresia_Click(object sender, EventArgs e)
        {
            indiceMenu = 3;
            this.Close();
        }

        private void membresiasMembresia_Click(object sender, EventArgs e)
        {
            indiceMenu = 4;
            this.Close();
        }

        private void asistenciaMembresia_Click(object sender, EventArgs e)
        {
            indiceMenu = 5;
            this.Close();
        }

        private void pagosMembresia_Click(object sender, EventArgs e)
        {
            indiceMenu = 6;
            this.Close();
        }

        private void homeMembresia_Click(object sender, EventArgs e)
        {
            indiceMenu = 7;
            this.Close();
        }
    }
}
