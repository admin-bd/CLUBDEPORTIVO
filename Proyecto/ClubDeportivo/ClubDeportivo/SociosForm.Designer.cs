﻿namespace ClubDeportivo
{
    partial class SociosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SociosForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.sociosSocio = new System.Windows.Forms.ToolStripButton();
            this.clasesSocio = new System.Windows.Forms.ToolStripButton();
            this.instructoresSocio = new System.Windows.Forms.ToolStripButton();
            this.membresiasSocio = new System.Windows.Forms.ToolStripButton();
            this.asistenciaSocio = new System.Windows.Forms.ToolStripButton();
            this.pagosSocio = new System.Windows.Forms.ToolStripButton();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(70)))), ((int)(((byte)(79)))));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(55, 50);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sociosSocio,
            this.clasesSocio,
            this.instructoresSocio,
            this.membresiasSocio,
            this.asistenciaSocio,
            this.pagosSocio});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(60, 470);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // sociosSocio
            // 
            this.sociosSocio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.sociosSocio.Image = ((System.Drawing.Image)(resources.GetObject("sociosSocio.Image")));
            this.sociosSocio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.sociosSocio.Name = "sociosSocio";
            this.sociosSocio.Size = new System.Drawing.Size(57, 54);
            this.sociosSocio.Text = "SOCIOS";
            this.sociosSocio.Click += new System.EventHandler(this.sociosSocio_Click);
            // 
            // clasesSocio
            // 
            this.clasesSocio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clasesSocio.Image = ((System.Drawing.Image)(resources.GetObject("clasesSocio.Image")));
            this.clasesSocio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clasesSocio.Name = "clasesSocio";
            this.clasesSocio.Size = new System.Drawing.Size(57, 54);
            this.clasesSocio.Text = "CLASES";
            this.clasesSocio.Click += new System.EventHandler(this.clasesSocio_Click);
            // 
            // instructoresSocio
            // 
            this.instructoresSocio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.instructoresSocio.Image = ((System.Drawing.Image)(resources.GetObject("instructoresSocio.Image")));
            this.instructoresSocio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.instructoresSocio.Name = "instructoresSocio";
            this.instructoresSocio.Size = new System.Drawing.Size(57, 54);
            this.instructoresSocio.Text = "INSTRUCTORES";
            this.instructoresSocio.Click += new System.EventHandler(this.instructoresSocio_Click);
            // 
            // membresiasSocio
            // 
            this.membresiasSocio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.membresiasSocio.Image = ((System.Drawing.Image)(resources.GetObject("membresiasSocio.Image")));
            this.membresiasSocio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.membresiasSocio.Name = "membresiasSocio";
            this.membresiasSocio.Size = new System.Drawing.Size(57, 54);
            this.membresiasSocio.Text = "MEMBRESIAS";
            this.membresiasSocio.Click += new System.EventHandler(this.membresiasSocio_Click);
            // 
            // asistenciaSocio
            // 
            this.asistenciaSocio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.asistenciaSocio.Image = ((System.Drawing.Image)(resources.GetObject("asistenciaSocio.Image")));
            this.asistenciaSocio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.asistenciaSocio.Name = "asistenciaSocio";
            this.asistenciaSocio.Size = new System.Drawing.Size(57, 54);
            this.asistenciaSocio.Text = "ASISTENCIA";
            this.asistenciaSocio.Click += new System.EventHandler(this.asistenciaSocio_Click);
            // 
            // pagosSocio
            // 
            this.pagosSocio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pagosSocio.Image = ((System.Drawing.Image)(resources.GetObject("pagosSocio.Image")));
            this.pagosSocio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pagosSocio.Name = "pagosSocio";
            this.pagosSocio.Size = new System.Drawing.Size(57, 54);
            this.pagosSocio.Text = "PAGOS";
            this.pagosSocio.Click += new System.EventHandler(this.pagosSocio_Click);
            // 
            // SociosForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(750, 470);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SociosForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SociosForm";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton sociosSocio;
        private System.Windows.Forms.ToolStripButton clasesSocio;
        private System.Windows.Forms.ToolStripButton instructoresSocio;
        private System.Windows.Forms.ToolStripButton membresiasSocio;
        private System.Windows.Forms.ToolStripButton asistenciaSocio;
        private System.Windows.Forms.ToolStripButton pagosSocio;
    }
}