﻿namespace ClubDeportivo
{
    partial class MembresiasForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MembresiasForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.sociosMembresia = new System.Windows.Forms.ToolStripButton();
            this.clasesMembresia = new System.Windows.Forms.ToolStripButton();
            this.instructoresMembresia = new System.Windows.Forms.ToolStripButton();
            this.membresiasMembresia = new System.Windows.Forms.ToolStripButton();
            this.asistenciaMembresia = new System.Windows.Forms.ToolStripButton();
            this.pagosMembresia = new System.Windows.Forms.ToolStripButton();
            this.homeMembresia = new System.Windows.Forms.ToolStripButton();
            this.cLUBDEPORTIVODataSet = new ClubDeportivo.CLUBDEPORTIVODataSet();
            this.membresiaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.membresiaTableAdapter = new ClubDeportivo.CLUBDEPORTIVODataSetTableAdapters.MembresiaTableAdapter();
            this.tableAdapterManager = new ClubDeportivo.CLUBDEPORTIVODataSetTableAdapters.TableAdapterManager();
            this.membresiaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.insertarMembresia = new System.Windows.Forms.Button();
            this.actualizarMembresia = new System.Windows.Forms.Button();
            this.eliminarMembresia = new System.Windows.Forms.Button();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cLUBDEPORTIVODataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.membresiaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.membresiaDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(70)))), ((int)(((byte)(79)))));
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(55, 50);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sociosMembresia,
            this.clasesMembresia,
            this.instructoresMembresia,
            this.membresiasMembresia,
            this.asistenciaMembresia,
            this.pagosMembresia,
            this.homeMembresia});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(174, 741);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // sociosMembresia
            // 
            this.sociosMembresia.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sociosMembresia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.sociosMembresia.Image = ((System.Drawing.Image)(resources.GetObject("sociosMembresia.Image")));
            this.sociosMembresia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.sociosMembresia.Name = "sociosMembresia";
            this.sociosMembresia.Size = new System.Drawing.Size(171, 54);
            this.sociosMembresia.Text = "Socios";
            this.sociosMembresia.Click += new System.EventHandler(this.sociosMembresia_Click);
            // 
            // clasesMembresia
            // 
            this.clasesMembresia.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clasesMembresia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.clasesMembresia.Image = ((System.Drawing.Image)(resources.GetObject("clasesMembresia.Image")));
            this.clasesMembresia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clasesMembresia.Name = "clasesMembresia";
            this.clasesMembresia.Size = new System.Drawing.Size(171, 54);
            this.clasesMembresia.Text = "Clases";
            this.clasesMembresia.Click += new System.EventHandler(this.clasesMembresia_Click);
            // 
            // instructoresMembresia
            // 
            this.instructoresMembresia.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.instructoresMembresia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.instructoresMembresia.Image = ((System.Drawing.Image)(resources.GetObject("instructoresMembresia.Image")));
            this.instructoresMembresia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.instructoresMembresia.Name = "instructoresMembresia";
            this.instructoresMembresia.Size = new System.Drawing.Size(171, 54);
            this.instructoresMembresia.Text = "Instructores";
            this.instructoresMembresia.Click += new System.EventHandler(this.instructoresMembresia_Click);
            // 
            // membresiasMembresia
            // 
            this.membresiasMembresia.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.membresiasMembresia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.membresiasMembresia.Image = ((System.Drawing.Image)(resources.GetObject("membresiasMembresia.Image")));
            this.membresiasMembresia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.membresiasMembresia.Name = "membresiasMembresia";
            this.membresiasMembresia.Size = new System.Drawing.Size(171, 54);
            this.membresiasMembresia.Text = "Membresías";
            this.membresiasMembresia.Click += new System.EventHandler(this.membresiasMembresia_Click);
            // 
            // asistenciaMembresia
            // 
            this.asistenciaMembresia.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asistenciaMembresia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.asistenciaMembresia.Image = ((System.Drawing.Image)(resources.GetObject("asistenciaMembresia.Image")));
            this.asistenciaMembresia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.asistenciaMembresia.Name = "asistenciaMembresia";
            this.asistenciaMembresia.Size = new System.Drawing.Size(171, 54);
            this.asistenciaMembresia.Text = "Asistencia";
            this.asistenciaMembresia.Click += new System.EventHandler(this.asistenciaMembresia_Click);
            // 
            // pagosMembresia
            // 
            this.pagosMembresia.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pagosMembresia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.pagosMembresia.Image = ((System.Drawing.Image)(resources.GetObject("pagosMembresia.Image")));
            this.pagosMembresia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pagosMembresia.Name = "pagosMembresia";
            this.pagosMembresia.Size = new System.Drawing.Size(171, 54);
            this.pagosMembresia.Text = "Pagos";
            this.pagosMembresia.Click += new System.EventHandler(this.pagosMembresia_Click);
            // 
            // homeMembresia
            // 
            this.homeMembresia.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.homeMembresia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.homeMembresia.Image = ((System.Drawing.Image)(resources.GetObject("homeMembresia.Image")));
            this.homeMembresia.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.homeMembresia.Name = "homeMembresia";
            this.homeMembresia.Size = new System.Drawing.Size(171, 54);
            this.homeMembresia.Text = "Home";
            this.homeMembresia.Click += new System.EventHandler(this.homeMembresia_Click);
            // 
            // cLUBDEPORTIVODataSet
            // 
            this.cLUBDEPORTIVODataSet.DataSetName = "CLUBDEPORTIVODataSet";
            this.cLUBDEPORTIVODataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // membresiaBindingSource
            // 
            this.membresiaBindingSource.DataMember = "Membresia";
            this.membresiaBindingSource.DataSource = this.cLUBDEPORTIVODataSet;
            // 
            // membresiaTableAdapter
            // 
            this.membresiaTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.AsistenciaClaseTableAdapter = null;
            this.tableAdapterManager.AsistenciaTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.ClasesTableAdapter = null;
            this.tableAdapterManager.HorariosTableAdapter = null;
            this.tableAdapterManager.InstructorTableAdapter = null;
            this.tableAdapterManager.MembresiaTableAdapter = this.membresiaTableAdapter;
            this.tableAdapterManager.PagosTableAdapter = null;
            this.tableAdapterManager.SocioTableAdapter = null;
            this.tableAdapterManager.UpdateOrder = ClubDeportivo.CLUBDEPORTIVODataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // membresiaDataGridView
            // 
            this.membresiaDataGridView.AutoGenerateColumns = false;
            this.membresiaDataGridView.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(171)))), ((int)(((byte)(172)))));
            this.membresiaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.membresiaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.membresiaDataGridView.DataSource = this.membresiaBindingSource;
            this.membresiaDataGridView.Location = new System.Drawing.Point(242, 184);
            this.membresiaDataGridView.Name = "membresiaDataGridView";
            this.membresiaDataGridView.Size = new System.Drawing.Size(962, 406);
            this.membresiaDataGridView.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "IdMembresia";
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "Descripcion";
            this.dataGridViewTextBoxColumn2.HeaderText = "Descripción";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "Precio";
            this.dataGridViewTextBoxColumn3.HeaderText = "Precio";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(70)))), ((int)(((byte)(79)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(584, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(195, 47);
            this.label1.TabIndex = 3;
            this.label1.Text = "Membresías";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // insertarMembresia
            // 
            this.insertarMembresia.BackColor = System.Drawing.Color.White;
            this.insertarMembresia.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.insertarMembresia.Location = new System.Drawing.Point(374, 108);
            this.insertarMembresia.Name = "insertarMembresia";
            this.insertarMembresia.Size = new System.Drawing.Size(135, 39);
            this.insertarMembresia.TabIndex = 4;
            this.insertarMembresia.Text = "Insertar";
            this.insertarMembresia.UseVisualStyleBackColor = false;
            this.insertarMembresia.Click += new System.EventHandler(this.insertarMembresia_Click);
            // 
            // actualizarMembresia
            // 
            this.actualizarMembresia.BackColor = System.Drawing.Color.White;
            this.actualizarMembresia.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.actualizarMembresia.Location = new System.Drawing.Point(611, 108);
            this.actualizarMembresia.Name = "actualizarMembresia";
            this.actualizarMembresia.Size = new System.Drawing.Size(135, 39);
            this.actualizarMembresia.TabIndex = 4;
            this.actualizarMembresia.Text = "Actualizar";
            this.actualizarMembresia.UseVisualStyleBackColor = false;
            this.actualizarMembresia.Click += new System.EventHandler(this.actualizarMembresia_Click);
            // 
            // eliminarMembresia
            // 
            this.eliminarMembresia.BackColor = System.Drawing.Color.White;
            this.eliminarMembresia.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eliminarMembresia.Location = new System.Drawing.Point(840, 108);
            this.eliminarMembresia.Name = "eliminarMembresia";
            this.eliminarMembresia.Size = new System.Drawing.Size(135, 39);
            this.eliminarMembresia.TabIndex = 4;
            this.eliminarMembresia.Text = "Eliminar";
            this.eliminarMembresia.UseVisualStyleBackColor = false;
            this.eliminarMembresia.Click += new System.EventHandler(this.eliminarMembresia_Click);
            // 
            // MembresiasForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(171)))), ((int)(((byte)(172)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.ControlBox = false;
            this.Controls.Add(this.eliminarMembresia);
            this.Controls.Add(this.actualizarMembresia);
            this.Controls.Add(this.insertarMembresia);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.membresiaDataGridView);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MembresiasForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Next Step - Sport Club";
            this.Load += new System.EventHandler(this.MembresiasForm_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cLUBDEPORTIVODataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.membresiaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.membresiaDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton sociosMembresia;
        private System.Windows.Forms.ToolStripButton clasesMembresia;
        private System.Windows.Forms.ToolStripButton instructoresMembresia;
        private System.Windows.Forms.ToolStripButton membresiasMembresia;
        private System.Windows.Forms.ToolStripButton asistenciaMembresia;
        private System.Windows.Forms.ToolStripButton pagosMembresia;
        private CLUBDEPORTIVODataSet cLUBDEPORTIVODataSet;
        private System.Windows.Forms.BindingSource membresiaBindingSource;
        private CLUBDEPORTIVODataSetTableAdapters.MembresiaTableAdapter membresiaTableAdapter;
        private CLUBDEPORTIVODataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView membresiaDataGridView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button insertarMembresia;
        private System.Windows.Forms.Button actualizarMembresia;
        private System.Windows.Forms.Button eliminarMembresia;
        private System.Windows.Forms.ToolStripButton homeMembresia;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
    }
}