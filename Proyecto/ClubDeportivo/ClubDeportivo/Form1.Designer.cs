﻿namespace ClubDeportivo
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.clasesBox = new System.Windows.Forms.PictureBox();
            this.sociosBox = new System.Windows.Forms.PictureBox();
            this.instructoresBox = new System.Windows.Forms.PictureBox();
            this.membresiasBox = new System.Windows.Forms.PictureBox();
            this.asistenciaBox = new System.Windows.Forms.PictureBox();
            this.pagosBox = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.clasesBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.sociosBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructoresBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.membresiasBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.asistenciaBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pagosBox)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(487, 62);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(343, 142);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // clasesBox
            // 
            this.clasesBox.BackColor = System.Drawing.Color.Transparent;
            this.clasesBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("clasesBox.BackgroundImage")));
            this.clasesBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.clasesBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.clasesBox.Location = new System.Drawing.Point(608, 261);
            this.clasesBox.Name = "clasesBox";
            this.clasesBox.Size = new System.Drawing.Size(108, 92);
            this.clasesBox.TabIndex = 1;
            this.clasesBox.TabStop = false;
            // 
            // sociosBox
            // 
            this.sociosBox.BackColor = System.Drawing.Color.Transparent;
            this.sociosBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("sociosBox.BackgroundImage")));
            this.sociosBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sociosBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.sociosBox.Location = new System.Drawing.Point(443, 261);
            this.sociosBox.Name = "sociosBox";
            this.sociosBox.Size = new System.Drawing.Size(93, 92);
            this.sociosBox.TabIndex = 1;
            this.sociosBox.TabStop = false;
            this.sociosBox.Click += new System.EventHandler(this.sociosBox_Click);
            // 
            // instructoresBox
            // 
            this.instructoresBox.BackColor = System.Drawing.Color.Transparent;
            this.instructoresBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("instructoresBox.BackgroundImage")));
            this.instructoresBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.instructoresBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.instructoresBox.Location = new System.Drawing.Point(778, 253);
            this.instructoresBox.Name = "instructoresBox";
            this.instructoresBox.Size = new System.Drawing.Size(112, 108);
            this.instructoresBox.TabIndex = 1;
            this.instructoresBox.TabStop = false;
            this.instructoresBox.Click += new System.EventHandler(this.instructoresBox_Click);
            // 
            // membresiasBox
            // 
            this.membresiasBox.BackColor = System.Drawing.Color.Transparent;
            this.membresiasBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("membresiasBox.BackgroundImage")));
            this.membresiasBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.membresiasBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.membresiasBox.Location = new System.Drawing.Point(443, 407);
            this.membresiasBox.Name = "membresiasBox";
            this.membresiasBox.Size = new System.Drawing.Size(93, 92);
            this.membresiasBox.TabIndex = 1;
            this.membresiasBox.TabStop = false;
            this.membresiasBox.Click += new System.EventHandler(this.membresiasBox_Click);
            // 
            // asistenciaBox
            // 
            this.asistenciaBox.BackColor = System.Drawing.Color.Transparent;
            this.asistenciaBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("asistenciaBox.BackgroundImage")));
            this.asistenciaBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.asistenciaBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.asistenciaBox.Location = new System.Drawing.Point(616, 407);
            this.asistenciaBox.Name = "asistenciaBox";
            this.asistenciaBox.Size = new System.Drawing.Size(93, 92);
            this.asistenciaBox.TabIndex = 1;
            this.asistenciaBox.TabStop = false;
            // 
            // pagosBox
            // 
            this.pagosBox.BackColor = System.Drawing.Color.Transparent;
            this.pagosBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pagosBox.BackgroundImage")));
            this.pagosBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pagosBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pagosBox.Location = new System.Drawing.Point(788, 407);
            this.pagosBox.Name = "pagosBox";
            this.pagosBox.Size = new System.Drawing.Size(93, 92);
            this.pagosBox.TabIndex = 1;
            this.pagosBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.label1.Location = new System.Drawing.Point(447, 356);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Socios";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.label2.Location = new System.Drawing.Point(620, 356);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(98, 31);
            this.label2.TabIndex = 2;
            this.label2.Text = "Clases";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.label3.Location = new System.Drawing.Point(762, 356);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 31);
            this.label3.TabIndex = 2;
            this.label3.Text = "Instructores";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.label4.Location = new System.Drawing.Point(416, 502);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 31);
            this.label4.TabIndex = 2;
            this.label4.Text = "Membresías";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.label5.Location = new System.Drawing.Point(600, 502);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 31);
            this.label5.TabIndex = 2;
            this.label5.Text = "Asistencia";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(244)))), ((int)(((byte)(255)))));
            this.label6.Location = new System.Drawing.Point(794, 502);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 31);
            this.label6.TabIndex = 2;
            this.label6.Text = "Pagos";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(70)))), ((int)(((byte)(79)))));
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1362, 741);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pagosBox);
            this.Controls.Add(this.asistenciaBox);
            this.Controls.Add(this.membresiasBox);
            this.Controls.Add(this.sociosBox);
            this.Controls.Add(this.instructoresBox);
            this.Controls.Add(this.clasesBox);
            this.Controls.Add(this.pictureBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Next Step - Sport Club";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.clasesBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.sociosBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.instructoresBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.membresiasBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.asistenciaBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pagosBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox clasesBox;
        private System.Windows.Forms.PictureBox sociosBox;
        private System.Windows.Forms.PictureBox instructoresBox;
        private System.Windows.Forms.PictureBox membresiasBox;
        private System.Windows.Forms.PictureBox asistenciaBox;
        private System.Windows.Forms.PictureBox pagosBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}

