﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClubDeportivo
{
    public partial class Form1 : Form
    {
        public int indice = 0;
        private SociosForm VentanaSocios;
        private MembresiasForm VentanaMembresia;
        private InstructoresForm ventanaInstructores;

        public Form1()
        {
            InitializeComponent();
            VentanaSocios = new SociosForm();
            VentanaMembresia = new MembresiasForm();
            ventanaInstructores = new InstructoresForm();
    }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
        }

        private void sociosBox_Click(object sender, EventArgs e)
        {
            VentanaSocios.ShowDialog();
            indice = VentanaSocios.indiceMenu;
            if (indice != 0)
                abreMenu();
        }

        private void membresiasBox_Click(object sender, EventArgs e)
        {
            VentanaMembresia.ShowDialog();
            indice = VentanaMembresia.indiceMenu;
            if (indice != 0)
                abreMenu();
        }

        private void abreMenu()
        {
            switch (indice)
            {
                case 1:
                    VentanaSocios.indiceMenu = 0;
                    VentanaSocios.ShowDialog();
                    indice = VentanaSocios.indiceMenu;
                    break;

                case 2: indice = 0;
                    break;

                case 3: ventanaInstructores.indiceMenu = 0;
                    ventanaInstructores.ShowDialog();
                    indice = ventanaInstructores.indiceMenu;
                    break;

                case 4:
                    VentanaMembresia.indiceMenu = 0;
                    VentanaMembresia.ShowDialog();
                    indice = VentanaMembresia.indiceMenu;
                    break;

                case 5: indice = 0;
                    break;

                case 6: indice = 0;
                    break;
                default: indice = 0;
                    break;
            }
            /*if (indice != 0)
            {
                abreMenu();
            }*/
        }

        private void instructoresBox_Click(object sender, EventArgs e)
        {
            ventanaInstructores.ShowDialog();
            indice = VentanaMembresia.indiceMenu;
            if (indice != 0)
                abreMenu();
        }
    }
}
